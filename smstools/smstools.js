// ============================================================================
// -  Copyright (C) 2016 Aedilis UAB.  All rights reserved.
// -
// -  Redistribution and use in source and binary forms, with or without
// -  modification, are permitted provided that the following conditions
// -  are met:
// -  1. Redistributions of source code must retain the above copyright
// -     notice, this list of conditions and the following disclaimer.
// -  2. Redistributions in binary form must reproduce the above
// -     copyright notice, this list of conditions and the following
// -     disclaimer in the documentation and/or other materials
// -     provided with the distribution.
// -
// -  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// -  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// -  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// -  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL ANY
// -  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// -  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// -  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// -  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
// -  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// -  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// -  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ============================================================================

function swapBytes(buffer) {
    var l = buffer.length;
    if (l & 0x01) {
        throw new Error('Buffer length must be even');
    }
    for (var i = 0; i < l; i += 2) {
        var a = buffer[i];
        buffer[i] = buffer[i + 1];
        buffer[i + 1] = a;
    }
    return buffer;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function isLatin(string) {
    if (string.match(/[^\x20-\x7F]/)) {
        return false;
    }

    return true;
}

module.exports = {
    parse: function (filename) {
        var fs = require("fs");
        var contents_arr = fs.readFileSync(filename, 'utf8').split("\n");
        var content_start = 0;
        var res = {
        };

        for (var i = 0; i < contents_arr.length; i++) {
            var line = contents_arr[i].trim();
            if (line.length === 0) {
                content_start = i;
                break;
            }

            var parts = line.split(": ");
            if (parts.length === 2) {
                res[parts[0].toLowerCase()] = parts[1].trim();
            }
        }
        content_size = contents_arr.slice(content_start + 1).join("\n").length;

        if ('from' in res) {
            res.topic = parseInt(res.from);
            delete res.from;
        } else if ('to' in res) {
            res.topic = parseInt(res.to);
            delete res.to;
        }

        var encoding = "ISO";
        if ('alphabet' in res) {
            encoding = res.alphabet;
            delete res.alphabet;

        }

        switch (encoding) {
            case "UCS2":
                var hex_contents = fs.readFileSync(filename).slice(-1 * content_size);
                var buf = new Buffer(hex_contents, 'binary');
                swapBytes(buf);
                res.payload = buf.toString('ucs2');
                break;

            default:
                res.payload = contents_arr.slice(content_start + 1).join("\n");
        }

        if ('sent' in res) {
            res.sent = Date.parse("20" + res.sent);
        }

        if ('received' in res) {
            res.received = Date.parse("20" + res.received);
        }

        return res;
    },
    stringify: function (obj) {
        var res = "";
        var content = "";

        if ('payload' in obj) {
            content = obj.payload;
            delete obj.payload;
        }

        if ('topic' in obj) {
            obj.to = obj.topic;
            delete obj.topic;
        }

        // Replace timestamps with date strings
        if ('sent' in obj) {
            obj.sent = new Date(obj.sent).toISOString().replace('T', ' ').substr(2, 17);
        }

        if ('received' in obj) {
            obj.received = new Date(obj.received).toISOString().replace('T', ' ').substr(2, 17);
        }

        if (isLatin(content)) {
            obj.alphabet = "ISO";
        } else {
            obj.alphabet = "UCS2";
            var buf = new Buffer(content, 'ucs2');
            swapBytes(buf);
            content = buf.toString('binary');
        }

        for (var attr in obj) {
            // Ignore properties that start with underscore
            if (attr.substring(0, 1) === "_") {
                continue;
            }

            res += capitalizeFirstLetter(attr) + ": " + obj[attr] + "\n";
        }

        res += "\n" + content;

        return res;
    }
};